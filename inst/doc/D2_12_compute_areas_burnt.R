
#### name: D2_12_compute_areas_burnt ####
setwd(projectdir)
source(file.path(codedir, 'utils.R'))

#areas burnt taken from CH_ash_harvest_2009fire_v2_wade.xls, an excel file from John Stein.

ma.area.pre.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="mountain ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Pre_2009_Total_Area, Age_Class, sum)
  )[c(3,1,2)]
#str(ma.area.pre.2009.unharvested)
#ma.area.pre.2009.unharvested
## old growth       1939  1940-1982 
##   5833.917  99157.582  18709.887 

aa.area.pre.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="alpine ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Pre_2009_Total_Area, Age_Class, sum)
  )[c(3,1,2)]
## aa.area.pre.2009.unharvested
## old growth       1939  1940-1982 
##   100.1114 18117.8690  2305.1997 

ash.area.pre.2009.unharvested <-  ma.area.pre.2009.unharvested + aa.area.pre.2009.unharvested
## ash.area.pre.2009.unharvested
## old growth       1939  1940-1982 
##   5934.028 117275.451  21015.086 

ma.area.unburnt.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="mountain ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Unburnt_2009_Area,Age_Class,sum)
  )[c(3,1,2)]

aa.area.unburnt.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="alpine ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Unburnt_2009_Area,Age_Class,sum)
  )[c(3,1,2)]

ash.area.unburnt.2009.unharvested <-  ma.area.unburnt.2009.unharvested + aa.area.unburnt.2009.unharvested

ma.area.burnt.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="mountain ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Burnt_2009_Area,Age_Class,sum)
  )[c(3,1,2)]

aa.area.burnt.2009.unharvested <- with(
  ash.fire.matrix.df[ash.fire.matrix.df$Ash_Type=="alpine ash" &
                     ash.fire.matrix.df$harvested_Coupe=="unharvested",],
  tapply(Burnt_2009_Area,Age_Class,sum)
  )[c(3,1,2)]

ash.area.burnt.2009.unharvested <-  ma.area.burnt.2009.unharvested + aa.area.burnt.2009.unharvested

#3K hectares were salvaged logged, going to assume that they were logged in proportion to the area burnt in 2009
ash.area.burnt.2009.unharvested.adj <- ash.area.burnt.2009.unharvested *
  (
  sum(ash.area.burnt.2009.unharvested) - 3000
    ) / sum(ash.area.burnt.2009.unharvested)

#### In the next chunk of code there are several numeric vectors being sourced from the main script
area.2011.unharvested <- area_2011_unharvested

trp.harvest <- trp_harvest
avail.harvest <- avail_harvest

burnt.area.unlogged.2009 <- burnt_area_unlogged_2009

total.2011.area <- sum(c(
  area.2011.unharvested,
  burnt.area.unlogged.2009
  ))
