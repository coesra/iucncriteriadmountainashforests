---
title: IUCNcriteriaDmountainashforests Results of Running the Workflow
author: Wade Blanchard and Ivan Hanigan
output:
  html_document:
    toc: true
    theme: united
    number_sections: yes
documentclass: article
classoption: a4paper
---

# Introduction
- This is an Rmarkdown workflow
- There is an associated Kepler workflow
```{r echo = F, eval=F, results="hide"}  
library(rmarkdown)
# library(knitr)
library(xtable)
getwd()
dir()
#rmarkdown::render("main_D2.Rmd", "html_document")
library(knitr)
knit2html("main_D2.Rmd")
```
# Setup Project Directory, Dependencies and Data
## D2_00_init_projectdir
- This chunk performs basic housekeeping checks to ensure the workspace contains the required data and code
- Any required R packages are installed if they are not already 
```{r echo = T, eval=T, results="hide"}
### Set the working directory
projectdir <- "~/KeplerData/workflows/MyWorkflows/IUCNcriteriaDmountainashforests"
setwd(projectdir)
indir <- projectdir
#codedir <- "~/KeplerData/workflows/MyWorkflows/IUCNcriteriaDmountainashforests/code"
codedir <- 'file.path(system.file(package="IUCNcriteriaDmountainashforests"), "doc")'
    
#### name:D2_00_init_projectdir.R ####
# convert codedir if system call from kepler string component, otherwise treat as dir path
if(grep("system.file", codedir)){
  codedir <- eval(parse(text = codedir))
}

eval <- T
if(eval){
source(file.path(codedir,"D2_00_init_projectdir.R"))
save.image(file.path(projectdir, ".RData"))
}
outdir <- projectdir
```

## D2_00_setup_data
- This chunk sets the directory name and the file names for required datasets
- It then sources a script that will read in the data and subset these into the required structure for later processing
- The stag data are subsetted by year (yy) and fire type (xx). Years were 1998, 2005, 2009, 2010, 2011.  
- The 2009 measurement occured after the 2009 fire
- TODO we will need to provide some basic metadata for these files.

```{r echo=T, eval = FALSE, results='hide'}
##### Set the directory to source the data from ####
datadir <- "~/data/IUCNmountainashforests_data/raw_data/stag_and_ash_data"
# NB these data are not supplied with the R package
infile_stag <- "stag_data_all_years.csv"
infile_ash <- "ash_fire_matrix.csv"

#### name: D2_00_setup_data ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_00_setup_data.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2_01_estimate_the_key_biotic_variable 
- This script Estimates the key biotic variable (mean number of stags per site)
- This is hard coded to conform to the Mountain Ash Stags variable. 
- to make this more generalised it would be required to swap the D2_01_estimate_the_key_biotic_variable.R script with something more generic if possible, or an alternative script for another biotic variable such as trophic diversity or others as recommended in 'Rodriguez, J. P., Keith, D. a., Rodriguez-Clark, K. M., Murray, N. J., Nicholson, E., Regan, T. J., … Wit, P. (2015). A practical guide to the application of the IUCN Red List of Ecosystems criteria S3. Philosophical Transactions of the Royal Society B: Biological Sciences, 370(1662), 20140003–20140003. doi:10.1098/rstb.2014.0003'
- Currently this chunk computes things for the subsets as set up previously
- First the average number of stags standing per site (form.num<=8) prior to the fire. we compute this by first counting the number of stags per site (the inner tapply) and then average this across the sites of a given category.
- Second compute the variance in the average number of stags per site (by age category and then by age category and burn status in 2009) this is essentially the standard error of the number of stags per site squared.
- TODO noticed the 2011 vars are computed by division with 2010, whereas the others are divided by their same year.  is that correct?


```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_01_estimate_the_key_biotic_variable ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_01_estimate_the_key_biotic_variable.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2_04_transition_probability_matrix
- Transition probability matrices are used in the study of Markov chains (discrete)
- Each row of the transition probability matrix sums to 1 and gives the probability of moving between the current state (row) to a new state (column)
- Note that, our transition probability matrices are upper triangular. That is, once a tree goes to a higher form it cannot return to a lower form.

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_04_transition_probability_matrix ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_04_transition_probability_matrix.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2_05_number_of_trees_by_form_in_2011_by_fire_category
- calculate the number of trees (frequencies) by form  in 2011 by fire category 
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_05_number_of_trees_by_form_in_2011_by_fire_category ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_05_number_of_trees_by_form_in_2011_by_fire_category.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## DISCOUNT FACTORS
- Use the transition probability matrices (tpm) and number of trees in each class (freq.11.nf.coll.ash) to compute the average number of stags (stems) in 2025, 2039, 2053 and 2067. 
- We have two tpms, one for sites unaffected by fire and one that experience a fire of similar intensity  to the 2009 fire (this is the only fire we have detailed transitions on).
- we vary the extent of the fire (small = 1983 fire, medium = 2009 fire and large = 1939 fire).

### D2_06_discount_factors_no_fire_f0000
- This uses the `mpower` function developed by this project.

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_06_discount_factors_no_fire_f0000 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_06_discount_factors_no_fire_f0000.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_07_discount_factors_fire_2011_2025_f1000
- DISCOUNT FACTORS-FIRE OCCURS in 2011-2025 f1000
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_07_discount_factors_fire_2011_2025_f1000 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_07_discount_factors_fire_2011_2025_f1000.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_08_discount_factors_fire_2025_2039_f0100
- DISCOUNT FACTORS-FIRE OCCURS in 2025-2039 f0100
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_08_discount_factors_fire_2025_2039_f0100 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_08_discount_factors_fire_2025_2039_f0100.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_09_discount_factors_fire_2039_2053_f0010
- DISCOUNT FACTORS-FIRE OCCURS in 2039-2053 f0010
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_09_discount_factors_fire_2039_2053_f0010 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_09_discount_factors_fire_2039_2053_f0010.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_10_discount_factors_fire_2053_2067_f0001
- DISCOUNT FACTORS-FIRE OCCURS in 2053-2067 f0001
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_10_discount_factors_fire_2053_2067_f0001 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_10_discount_factors_fire_2053_2067_f0001.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2 Burning and Harvesting Regimes
### D2_11_setup_areas_burnt (USERS MAY EDIT THIS)
- this actor has many computations based on the input data,  but these numbers appear to be inputs that might be interesting to vary?
- NB in kepler these could be made as inputs and we\'d offer users to edit their values....  but at the moment they are just set up in the script
- `area_2011_unharvested`  the order is old growth, 1939, 1983 in hectares
- `area_2011_unharvested` <- c(1713,51619,11185)
- `trp_harvest` the order is old growth, 1939, 1983 in hectares
- `trp_harvest` <- c(0,586,151)
- `avail_harvest` the order is old growth, 1939, 1983 in hectares
- `avail_harvest` <- c(0,21741,7360)
- TODO: I propose we remove this statement from the original code? "not going to use the data from 1983 at this point. need to clarify with Emma, John, David"

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_11_setup_areas_burnt ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_11_setup_areas_burnt.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_12_compute_areas_burnt
- NB areas burnt taken from CH_ash_harvest_2009fire_v2_wade.xls, an excel file from John Stein.
```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_12_compute_areas_burnt ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_12_compute_areas_burnt.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_13_fire_extents
- unlike the previous step were  user defined variables are defined in a script,
- here they are passed from the Kepler interface 
- it is unclear the best way to do this.
```{r echo=T, eval = FALSE, results='hide'}
#### Kepler Actor = fire_extents
fire_mult_1939 <- 0.9575470 - 0.11
fire_mult_1983 <- 0.1094486
fire_mult_2009 <- 0.3385079  

#### name: D2_13_fire_extents ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_13_fire_extents.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_14_baseline
- this gives `projection.array`, 3 harvesting regimes X 13 fire regimes X 4 time intervals, and
- the number of stags per hectare in 2011, the baseline we are using to project future collapse

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_14_baseline ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_14_baseline.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_15_harvesting_regime_1
- harvesting regime 1, no harvesting, and no fire
- this uses the functions `no.har.no.fire`, `fire.fun.no.logging` and `stems.per.hectare.fun` created for this project
- TODO the implementation in the script involves a lot of duplicated lines of code that might be streamlined somehow.  Perhaps in a loop?  

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_15_harvesting_regime_1 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_15_harvesting_regime_1.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_16_harvesting_regime_2
- harvesting scenario 2, 1939 and 1983 regrowth harvesting
- this section utilises the functions `har.no.fire` and `logging.fun.with.fire` developed for this project

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_16_harvesting_regime_2 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_16_harvesting_regime_2.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

### D2_17_harvesting_regime_3
- harvesting regime 3: logging 1983 regrowth only

```{r echo=T, eval = FALSE, results='hide'}
#### name: D2_17_harvesting_regime_3 ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_17_harvesting_regime_3.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2_18_compute_projection_scenarios
- this is projection for 2067, the last column of the projection array

```{r echo=T, eval = T, results='asis'}
#### name: D2_18_compute_projection_scenarios ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_18_compute_projection_scenarios.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

## D2_19_projection_scenarios_sensitivity_and_combined_summary

```{r echo=T, eval = T, results='asis'}
#### name: D2_19_projection_scenarios_sensitivity_and_combined_summary ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'D2_19_projection_scenarios_sensitivity_and_combined_summary.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

# Provenance Information
```{r}
Sys.Date()
sessionInfo()
```
