
#### name: D2_11_setup_areas_burnt ####
setwd(projectdir)
source(file.path(codedir, 'utils.R'))

# this actor has many computations based on the input data, 
# but these numbers appear to be inputs that might be interesting to vary?
# NB in kepler these could be made as inputs and we'd offer users to edit
# their values....  but at the moment they are just set up in the script

#### area_2011_unharvested 
#### These numbers came from 
# og_39_83_trp_res(area_ha)revised.xlsx
# the order is old growth, 1939, 1983 in hectares
area_2011_unharvested <- c(1713,51619,11185)

#### trp_harvest
#### logging by year, according to the Timber release program,
# 581ha/year in 1939 regrowth,
# 193ha/year in 1983 regrowth, and
# 0 in oldgrowh
# the order is old growth, 1939, 1983 in hectares
trp_harvest <- c(0,586,151)

#### avail_harvest
# the order is old growth, 1939, 1983 in hectares
avail_harvest <- c(0,21741,7360)

#### burnt_area_unlogged_2009
#area projections:
# using 21355ha for 1939 regrowth and 8504ha for 1983 regrowth the
#amount of hectares available for logging from table 3 of the
#manuscript, burnt and not logged in 2009, not going to use the data
#from 1983 at this point
# TODO: I propose we remove this statement from the original code?
# "need to clarify with Emma, John, David"
# the order is old growth, 1939, 1983 in hectares
burnt_area_unlogged_2009 <- c(3100, 35350, 53400-3100-35350)
#save.image(file.path(indir,".RData"))
#outdir <- indir
