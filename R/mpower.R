
#' @title mpower is a function written for this project
#' @name mpower
#' @param M
#' @param p
#' @return R

mpower<- function (M, p)
{
    A = as.matrix(M)
    if (dim(A)[1] != dim(A)[2])
        stop("not a square matrix")
    if (p == -1)
        return(solve(A))
    if (p == 0)
        return(diag(1, dim(A)[1], dim(A)[2]))
    if (p == 1)
        return(A)
    if (p < -1)
        stop("only powers >= -1 allowed")
    if (p != as.integer(p))
        stop("only integer powers allowed")
    R = A
    for (i in 2:p) {
        R = R %*% A
    }
    return(R)
}
