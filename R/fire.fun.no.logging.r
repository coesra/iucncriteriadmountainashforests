
################################################################
# name:fire.fun.no.logging
#similar to the previous function except with no logging, this represents the no future logging scenario.

fire.fun.no.logging<-function(interval,fire.mult,burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=c(1713,51619,11185))
{

#assuming the fire happens at the same time as the 2009 fire within the 14 year window.
#assuming the salvage logging happens in proportion to the size of the timber release program.
if (interval ==1 ) {
available1<-area.unburned
unlogged.unburnt.area1<-(available1)*(1-fire.mult)
unlogged.burnt.area1<-(available1)*fire.mult
burnt.area.unlogged.unburnt1<-burnt.area.unlogged*(1-fire.mult)
burnt.area.unlogged.burnt.again1<-burnt.area.unlogged*fire.mult

available2.4<-unlogged.unburnt.area1
unlogged.unburnt.area2.4<-rep(available2.4,3)
unlogged.burnt.area2.4<-rep(unlogged.burnt.area1,3)
burnt.area.unlogged.unburnt2.4<-rep(burnt.area.unlogged.unburnt1,3)
burnt.area.unlogged.burnt.again2.4<-rep(burnt.area.unlogged.burnt.again1,3)

unlogged.unburnt.area<-c(unlogged.unburnt.area1,unlogged.unburnt.area2.4)
unlogged.burnt.area<-c(unlogged.burnt.area1,unlogged.burnt.area2.4)
burnt.area.unlogged.unburnt<-c(burnt.area.unlogged.unburnt1,burnt.area.unlogged.unburnt2.4)
burnt.area.unlogged.burnt.again<-c(burnt.area.unlogged.burnt.again1,burnt.area.unlogged.burnt.again2.4)
}
else{
if (interval==2) {
available1<-area.unburned
available2<-available1

unlogged.unburnt.area1<-available1
unlogged.burnt.area1<- rep(0,3)
burnt.area.unlogged.unburnt1<-burnt.area.unlogged
burnt.area.unlogged.burnt.again1<-rep(0,3)

unlogged.unburnt.area2<-(available2)*(1-fire.mult)
unlogged.burnt.area2<-(available2)*fire.mult
burnt.area.unlogged.unburnt2<-burnt.area.unlogged*(1-fire.mult)
burnt.area.unlogged.burnt.again2<-burnt.area.unlogged*fire.mult

available3.4<-unlogged.unburnt.area2
unlogged.unburnt.area3.4<-rep(available3.4,2)
unlogged.burnt.area3.4<-rep(unlogged.burnt.area2,2)
burnt.area.unlogged.unburnt3.4<-rep(burnt.area.unlogged.unburnt2,2)
burnt.area.unlogged.burnt.again3.4<-rep(burnt.area.unlogged.burnt.again2,2)

unlogged.unburnt.area<-c(unlogged.unburnt.area1,unlogged.unburnt.area2,unlogged.unburnt.area3.4)
unlogged.burnt.area<-c(unlogged.burnt.area1,unlogged.burnt.area2,unlogged.burnt.area3.4)
burnt.area.unlogged.unburnt<-c(burnt.area.unlogged.unburnt1,burnt.area.unlogged.unburnt2,burnt.area.unlogged.unburnt3.4)
burnt.area.unlogged.burnt.again<-c(burnt.area.unlogged.burnt.again1,burnt.area.unlogged.burnt.again2,burnt.area.unlogged.burnt.again3.4)
}
else{
if (interval==3) {

available1.2<-rep(area.unburned,2)
available3<-available1.2[4:6]

unlogged.unburnt.area1.2<-available1.2
unlogged.burnt.area1.2<- rep(0,6)
burnt.area.unlogged.unburnt1.2<-rep(burnt.area.unlogged,2)
burnt.area.unlogged.burnt.again1.2<-rep(0,6)

unlogged.unburnt.area3<-(available3)*(1-fire.mult)
unlogged.burnt.area3<-(available3)*fire.mult
burnt.area.unlogged.unburnt3<-burnt.area.unlogged*(1-fire.mult)
burnt.area.unlogged.burnt.again3<-burnt.area.unlogged*fire.mult

available4<-unlogged.unburnt.area3

unlogged.unburnt.area4<-available4
unlogged.burnt.area4<-rep(unlogged.burnt.area3,1)
burnt.area.unlogged.unburnt4<-rep(burnt.area.unlogged.unburnt3,1)
burnt.area.unlogged.burnt.again4<-rep(burnt.area.unlogged.burnt.again3,1)

unlogged.unburnt.area<-c(unlogged.unburnt.area1.2,unlogged.unburnt.area3,unlogged.unburnt.area4)
unlogged.burnt.area<-c(unlogged.burnt.area1.2,unlogged.burnt.area3,unlogged.burnt.area4)
burnt.area.unlogged.unburnt<-c(burnt.area.unlogged.unburnt1.2,burnt.area.unlogged.unburnt3,burnt.area.unlogged.unburnt4)
burnt.area.unlogged.burnt.again<-c(burnt.area.unlogged.burnt.again1.2,burnt.area.unlogged.burnt.again3,burnt.area.unlogged.burnt.again4)
}
else{
if (interval==4) {

available1.3<-rep(area.unburned,3)
available4<-available1.3[7:9]

unlogged.unburnt.area1.3<-available1.3
unlogged.burnt.area1.3<- rep(0,9)
burnt.area.unlogged.unburnt1.3<-rep(burnt.area.unlogged,3)
burnt.area.unlogged.burnt.again1.3<-rep(0,9)

unlogged.unburnt.area4<-(available4)*(1-fire.mult)
unlogged.burnt.area4<-(available4)*fire.mult
burnt.area.unlogged.unburnt4<-burnt.area.unlogged*(1-fire.mult)
burnt.area.unlogged.burnt.again4<-burnt.area.unlogged*fire.mult

unlogged.unburnt.area<-c(unlogged.unburnt.area1.3,unlogged.unburnt.area4)
unlogged.burnt.area<-c(unlogged.burnt.area1.3,unlogged.burnt.area4)
burnt.area.unlogged.unburnt<-c(burnt.area.unlogged.unburnt1.3,burnt.area.unlogged.unburnt4)
burnt.area.unlogged.burnt.again<-c(burnt.area.unlogged.burnt.again1.3,burnt.area.unlogged.burnt.again4)

}
}
}
}

return(list(unlogged.unburnt=unlogged.unburnt.area,unlogged.burnt=unlogged.burnt.area,
burnt.area.unburnt=burnt.area.unlogged.unburnt,burnt.area.burnt.again=burnt.area.unlogged.burnt.again))
}
