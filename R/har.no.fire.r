
################################################################
# name:har.no.fire


har.no.fire<-function(no.stags.nf,no.stags.af,discount.fac,area.unharv.proj,burnt.unlogged,total.area)
{
# no.stags.nf = the average number of stags on sites that were unburned
# no.stags.af = the average number of stags on sites that were burned (any severity)
# discount.fac = the fraction of stags lost over time (computed using the probability transition matrices)
# area.unharv = the area in 2011 that was unharvested and unburnt in 2009
# burnt.unlogged= the area that was burnt in 2009 and unlogged
# total.area = the area of forest in 2011.

#note area.unharv.proj is a vector, stored in consecutive blocks of length 3.
ash.num<-c(
y2025=crossprod(no.stags.nf*discount.fac$y2025[1],area.unharv.proj[1:3])+
crossprod(no.stags.af*discount.fac$y2025[1],burnt.unlogged),
y2039=crossprod(no.stags.nf*discount.fac$y2039[1],area.unharv.proj[4:6])+
crossprod(no.stags.af*discount.fac$y2039[1],burnt.unlogged),
y2053=crossprod(no.stags.nf*discount.fac$y2053[1],area.unharv.proj[7:9])+
crossprod(no.stags.af*discount.fac$y2053[1],burnt.unlogged),
y2067=crossprod(no.stags.nf*discount.fac$y2067[1],area.unharv.proj[10:12])+
crossprod(no.stags.af*discount.fac$y2067[1],burnt.unlogged))


ash.num/total.area
}
